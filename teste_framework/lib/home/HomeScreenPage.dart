import 'package:flutter/material.dart';
import 'file:///C:/REPOSITORIOS/Pessoal/teste_framework/lib/album/AlbumsPage.dart';
import 'package:teste_framework/post/PostsPage.dart';
import 'package:teste_framework/todo/TodosPage.dart';

class HomeScreenPage extends StatefulWidget {
  static const routeName = '/home';

  @override
  _HomeScreenPageState createState() => _HomeScreenPageState();
}

class _HomeScreenPageState extends State<HomeScreenPage> {
  int _pageIndex = 0;
  final List<Widget> _screens = [PostsPage(), AlbumsPage(), TodosPage()];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text('Teste Framework'),),
        automaticallyImplyLeading: false,
      ),
      body: _screens[_pageIndex],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _pageIndex,
        onTap: onTabTapped,
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.list_alt), label: "POSTAGENS"),
          BottomNavigationBarItem(icon: Icon(Icons.album), label: "ALBUNS"),
          BottomNavigationBarItem(icon: Icon(Icons.list_alt), label: "TO-DOs"),
        ],
      ),
    );
  }

  void onTabTapped(int index) {
    setState(() {
      _pageIndex = index;
    });
  }
}
