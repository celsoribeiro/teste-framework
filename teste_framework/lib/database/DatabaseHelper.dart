import 'dart:io';

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper {

  static final _databaseName = "teste.db";
  static final _databaseVersion = 1;

  static final ALBUM = 'Album';
  static final TODO = 'Todo';
  static final POST = 'Post';

  static final columnId  = 'id';
  // make this a singleton class
  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  // only have a single app-wide reference to the database
  static Database _database;
  Future<Database> get database async {
    if (_database != null) return _database;
    // lazily instantiate the db the first time it is accessed
    _database = await _initDatabase();
    return _database;
  }

  // this opens the database (and creates it if it doesn't exist)
  _initDatabase() async {
    // Get a location using getDatabasesPath
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, _databaseName);

    return await openDatabase(path,
        version: _databaseVersion,
        onCreate: _onCreate);
  }

  // SQL code to create the database table
  Future _onCreate(Database db, int version) async {

    await db.execute("CREATE TABLE $ALBUM ("
        "$columnId integer,"
        "userId integer,"
        "title text)");

    await db.execute("CREATE TABLE $POST ("
        "$columnId integer,"
        "userId integer,"
        "title text,"
        "body text)");

    await db.execute("CREATE TABLE $TODO ("
        "$columnId integer,"
        "userId integer,"
        "title text,"
        "completed integer)");

  }

  Future<int> insert(String table,Map<String, dynamic> row) async {
    Database db = await instance.database;
    return await db.insert(table, row);
  }

  Future<List<Map<String, dynamic>>> queryAllRows(String table) async {
    Database db = await instance.database;
    return await db.query(table);
  }
  Future<List<Map<String, dynamic>>> executeQuery(String query) async {
    Database db = await instance.database;
    return await db.rawQuery(query);
  }

  Future<int> queryRowCount(String table) async {
    Database db = await instance.database;
    return Sqflite.firstIntValue(await db.rawQuery('SELECT COUNT(*) FROM $table'));
  }

  Future<int> delete(String table,int id) async {
    Database db = await instance.database;
    return await db.delete(table, where: '$columnId = ?', whereArgs: [id]);
  }
  Future<int> deleteAll(String table) async {
    Database db = await instance.database;
    return await db.delete(table);
  }
  Future<int> update(String table,Map<String, dynamic> row) async {
    Database db = await instance.database;
    int id = row[columnId];
    return await db.update(table, row, where: '$columnId = ?', whereArgs: [id]);
  }
}