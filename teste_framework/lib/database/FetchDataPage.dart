import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:teste_framework/database/DatabaseController.dart';
import 'package:teste_framework/home/HomeScreenPage.dart';
import 'package:teste_framework/model/Album.dart';
import 'package:teste_framework/model/Post.dart';
import 'package:teste_framework/model/Todo.dart';

class FetchDataPage extends StatefulWidget {
  static const routeName = '/fetch';

  FetchDataPage({Key key}) : super(key: key);

  @override
  _FetchDataPageState createState() => _FetchDataPageState();
}

class _FetchDataPageState extends State<FetchDataPage> {
  DatabaseController databaseController;
  bool isLoading;

  @override
  void initState() {
    super.initState();
    databaseController = DatabaseController();
    isLoading = true;
    checkFirstAccess();
  }

  void checkFirstAccess() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isFirstAccess = prefs.getBool('isFirstAccess') ?? true;
    if(isFirstAccess){
      getData();
    }else{
      setState(() {
        isLoading = false;
        Navigator.pushNamed(context, HomeScreenPage.routeName);
      });
    }
    await prefs.setBool('isFirstAccess', false);
  }
  void getData() async {
    await fetchAlbums();
    await fetchTodos();
    await fetchPosts();
    setState(() {
      isLoading = false;
      Navigator.pushNamed(context, HomeScreenPage.routeName);
    });
  }

  void fetchAlbums() async {
    try {
      Response response =
          await Dio().get("https://jsonplaceholder.typicode.com/albums");
      if (response.statusCode == 200) {
        List<Album> albuns =
            (response.data as List).map((x) => Album.fromJson(x)).toList();
        await databaseController.setAlbums(albuns);
      }
      print(response);
    } catch (e) {
      print(e);
    }
  }

  void fetchTodos() async {
    try {
      Response response =
          await Dio().get("https://jsonplaceholder.typicode.com/todos");
      if (response.statusCode == 200) {
        List<Todo> todos =
            (response.data as List).map((x) => Todo.fromJson(x)).toList();
        await databaseController.setTodos(todos);
      }
      print(response);
    } catch (e) {
      print(e);
    }
  }

  void fetchPosts() async {
    try {
      Response response =
          await Dio().get("https://jsonplaceholder.typicode.com/posts");
      if (response.statusCode == 200) {
        List<Post> posts =
            (response.data as List).map((x) => Post.fromJson(x)).toList();
        await databaseController.setPosts(posts);
      }
      print(response);
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: Center(
          child: isLoading
              ? Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CircularProgressIndicator(),
                    Container(
                        margin: const EdgeInsets.all(20.0),
                        child: new Text('sincronizando dados'))
                  ],
                )
              : Container(),
        ));
  }
}
