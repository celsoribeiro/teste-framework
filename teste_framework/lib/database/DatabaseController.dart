import 'package:teste_framework/model/Album.dart';
import 'package:teste_framework/model/Post.dart';
import 'package:teste_framework/model/Todo.dart';

import 'DatabaseHelper.dart';

class DatabaseController {
  DatabaseHelper dbHelper;

  DatabaseController() {
    dbHelper = DatabaseHelper.instance;
  }

  setAlbums(List<Album> list) async {
    await deleteAll(DatabaseHelper.ALBUM);
    for (Album doc in list) {
      // row to insert
      Map<String, dynamic> row = {
        'userId': doc.userId,
        'id': doc.id,
        'title': doc.title,
      };

      final insert = await dbHelper.insert(DatabaseHelper.ALBUM, row);
      print('inserted row: $insert');
    }
  }

  setPosts(List<Post> list) async {
    await deleteAll(DatabaseHelper.POST);
    for (Post doc in list) {
      // row to insert
      Map<String, dynamic> row = {
        'userId': doc.userId,
        'id': doc.id,
        'title': doc.title,
        'body': doc.body,
      };

      final insert = await dbHelper.insert(DatabaseHelper.POST, row);
      print('inserted row: $insert');
    }
  }

  setTodos(List<Todo> list) async {
    await deleteAll(DatabaseHelper.TODO);
    for (Todo doc in list) {
      // row to insert
      Map<String, dynamic> row = {
        'userId': doc.userId,
        'id': doc.id,
        'title': doc.title,
        'completed': doc.completed ? 1:0,
      };

      final insert = await dbHelper.insert(DatabaseHelper.TODO, row);
      print('inserted row: $insert');
    }
  }
  deleteAll(String table) async {
    final id = await dbHelper.deleteAll(table);
    return id;
  }

  Future<List<Album>> getAlbums() async {
    List<Album> response = List();
    List<Map> list = await dbHelper.queryAllRows(
        DatabaseHelper.ALBUM);

    for (var e in list) {
      response.add(Album.fromMap(e));
    }
    return response;
  }

  Future<List<Post>> getPosts() async {
    List<Post> response = List();
    List<Map> list = await dbHelper.queryAllRows(
        DatabaseHelper.POST);

    for (var e in list) {
      response.add(Post.fromMap(e));
    }
    return response;
  }

  Future<List<Todo>> getTodos() async {
    List<Todo> response = List();
    List<Map> list = await dbHelper.queryAllRows(
        DatabaseHelper.TODO);

    for (var e in list) {
      response.add(Todo.fromMap(e));
    }
    return response;
  }




}