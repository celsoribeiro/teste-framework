import 'dart:io';

import 'package:flutter/material.dart';
import 'package:teste_framework/database/DatabaseController.dart';
import 'package:teste_framework/model/Todo.dart';

import '../_theme.dart';

class TodosPage extends StatefulWidget {
  TodosPage({Key key}) : super(key: key);

  static const routeName = '/todos';
  String title = 'TO-DOs';

  @override
  _TodosPageState createState() => _TodosPageState();
}

class _TodosPageState extends State<TodosPage> {
  bool isLoading = false;
  List<Todo> todos = List();

  @override
  void initState() {
    super.initState();
    getTodos();
  }

  void getTodos() async {
    DatabaseController controller = DatabaseController();
    List<Todo> todos = await controller.getTodos();
    bindList(todos);
  }

  void bindList(List<Todo> todos) {
    setState(() {
      this.todos = todos;
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
            resizeToAvoidBottomInset: true,
            backgroundColor: MyColors.backgroundColor,
            body: isLoading
                ? CircularProgressIndicator()
                : Container(
              child: Padding(
                padding: EdgeInsets.only(
                    top: 8.0, left: 8.0, right: 8.0, bottom: 8.0),
                child: Container(
                  child: todos.length > 0
                      ? ListView.builder(
                      itemCount: todos.length,
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(child: row(index));
                      })
                      : Container(),
                ),
              ),
            )));
  }

  Container row(int index) {
    Todo todo = todos[index];

    return Container(
      margin: const EdgeInsets.symmetric(vertical: 5.0),
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          Container(child: Text(
            todo.title,
            style: TextStyle(color: MyColors.title),
          ),margin: EdgeInsets.only(bottom: 8.0),),
          todo.completed?Text('Done', style: TextStyle(color: Colors.green))
          :Text('In Progress', style: TextStyle(color: MyColors.text))
        ],
      ),
      decoration: BoxDecoration(
        color: MyColors.card,
        borderRadius: BorderRadius.circular(8.0),
        boxShadow: <BoxShadow>[
          new BoxShadow(
              color: Colors.black12,
              blurRadius: 10.0,
              offset: new Offset(0.0, 5.0))
        ],
      ),
    );
  }
}
