import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:teste_framework/database/DatabaseController.dart';
import 'package:teste_framework/model/Post.dart';

import '../_theme.dart';

class PostsPage extends StatefulWidget {
  PostsPage({Key key}) : super(key: key);

  static const routeName = '/posts';
  String title = 'Posts';

  @override
  _PostsPageState createState() => _PostsPageState();
}

class _PostsPageState extends State<PostsPage> {
  bool isLoading = false;
  List<Post> posts = List();

  @override
  void initState() {
    super.initState();
    getPosts();
  }

  void getPosts() async {
    DatabaseController controller = DatabaseController();
    List<Post> posts = await controller.getPosts();
    bindList(posts);
    print(posts);
  }

  void bindList(List<Post> posts) {
    setState(() {
      this.posts = posts;
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
            resizeToAvoidBottomInset: true,
            backgroundColor: MyColors.backgroundColor,
            body: isLoading
                ? CircularProgressIndicator()
                : Container(
                    child: Padding(
                      padding: EdgeInsets.only(
                          top: 8.0, left: 8.0, right: 8.0, bottom: 8.0),
                      child: Container(
                        child: posts.length > 0
                            ? ListView.builder(
                                itemCount: posts.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return GestureDetector(child: row(index));
                                })
                            : Container(),
                      ),
                    ),
                  )));
  }

  Container row(int index) {
    Post post = posts[index];

    return Container(
      margin: const EdgeInsets.symmetric(vertical: 5.0),
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(child: Text(
            post.title,
            style: TextStyle(color: MyColors.title),
          ),margin: EdgeInsets.only(bottom: 8.0),),

          Text(post.body, style: TextStyle(color: MyColors.text))
        ],
      ),
      decoration: BoxDecoration(
        color: MyColors.card,
        borderRadius: BorderRadius.circular(8.0),
        boxShadow: <BoxShadow>[
          new BoxShadow(
              color: Colors.black12,
              blurRadius: 10.0,
              offset: new Offset(0.0, 5.0))
        ],
      ),
    );
  }
}
