import 'dart:io';

import 'package:flutter/material.dart';
import 'package:teste_framework/database/DatabaseController.dart';
import 'package:teste_framework/model/Album.dart';

import '../_theme.dart';

class AlbumsPage extends StatefulWidget {
  AlbumsPage({Key key}) : super(key: key);

  static const routeName = '/albuns';
  String title = 'Albums';

  @override
  _AlbumsPageState createState() => _AlbumsPageState();
}

class _AlbumsPageState extends State<AlbumsPage> {
  bool isLoading = false;
  List<Album> albums = List();

  @override
  void initState() {
    super.initState();
    getAlbums();
  }
  void getAlbums() async {
    DatabaseController controller = DatabaseController();
    List<Album> albums = await controller.getAlbums();
    bindList(albums);

  }

  void bindList(List<Album> albums) {
    setState(() {
      this.albums = albums;
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
            resizeToAvoidBottomInset: true,
            backgroundColor: MyColors.backgroundColor,
            body: isLoading
                ? CircularProgressIndicator()
                : Container(
              child: Padding(
                padding: EdgeInsets.only(
                    top: 8.0, left: 8.0, right: 8.0, bottom: 8.0),
                child: Container(
                  child: albums.length > 0
                      ? ListView.builder(
                      itemCount: albums.length,
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(child: row(index));
                      })
                      : Container(),
                ),
              ),
            )));
  }

  Container row(int index) {
    Album album = albums[index];

    return Container(
      margin: const EdgeInsets.symmetric(vertical: 5.0),
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          Container(child: Text(
            album.title,
            style: TextStyle(color: MyColors.title),
          ),margin: EdgeInsets.only(bottom: 8.0),),
        ],
      ),
      decoration: BoxDecoration(
        color: MyColors.card,
        borderRadius: BorderRadius.circular(8.0),
        boxShadow: <BoxShadow>[
          new BoxShadow(
              color: Colors.black12,
              blurRadius: 10.0,
              offset: new Offset(0.0, 5.0))
        ],
      ),
    );
  }

}
