import 'package:flutter/material.dart';
import 'package:teste_framework/_theme.dart';
import 'file:///C:/REPOSITORIOS/Pessoal/teste_framework/lib/album/AlbumsPage.dart';
import 'file:///C:/REPOSITORIOS/Pessoal/teste_framework/lib/post/PostsPage.dart';
import 'file:///C:/REPOSITORIOS/Pessoal/teste_framework/lib/todo/TodosPage.dart';
import 'package:teste_framework/database/FetchDataPage.dart';
import 'package:teste_framework/home/HomeScreenPage.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
// This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Teste Framework',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: FetchDataPage.routeName,
      routes: {
        FetchDataPage.routeName: (context) => FetchDataPage(),
        HomeScreenPage.routeName: (context) => HomeScreenPage(),
        PostsPage.routeName: (context) => PostsPage(),
        AlbumsPage.routeName: (context) => AlbumsPage(),
        TodosPage.routeName: (context) => TodosPage(),
      },
    );
  }
}
