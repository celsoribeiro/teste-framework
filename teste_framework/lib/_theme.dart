import 'dart:ui';
import 'package:flutter/material.dart';

class MyColors {

  static const Color text = Color(0xffd9d9d9);
  static const Color card = Color(0xFF0F1028);
  static const Color backgroundColor = Color(0xFF0A0A1A);
  static const Color title = Color(0xFF04c4fc);

}



